import ptBr from './languages/pt-br.json';
import esPe from './languages/es-pe.json';
import enUs from './languages/en-us.json';

export const defaultLocale = 'pt-br';

export const languages = {
  'pt-br': ptBr,
  es: esPe,
  en: enUs,
};
